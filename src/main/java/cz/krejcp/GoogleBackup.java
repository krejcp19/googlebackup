package cz.krejcp;

import cz.krejcp.config.ConfigDto;
import cz.krejcp.config.ConfigReader;
import cz.krejcp.service.CleanAndUploadFileSynchronizer;
import cz.krejcp.service.FileSynchronizer;
import cz.krejcp.service.file.FileService;
import cz.krejcp.service.file.FileSystemService;
import cz.krejcp.service.file.GoogleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;


public class GoogleBackup {
    private static final Logger LOG = LoggerFactory.getLogger(GoogleBackup.class);
    private final FileService fsService;
    private final FileSynchronizer fileSynchronizer;
    private final FileService googleService;
    private ConfigDto configDto;

    public static void main(String[] args) throws IOException, GeneralSecurityException {
        if(args.length != 1) {
            throw new IllegalArgumentException("one argument expected {path to config}");
        }
        GoogleBackup backup = new GoogleBackup(args[0]);
        backup.run();
    }

    public GoogleBackup(String configPath) throws IOException, GeneralSecurityException {
        ConfigReader reader = new ConfigReader();
        fsService = new FileSystemService();
        fileSynchronizer = new CleanAndUploadFileSynchronizer();
        configDto = reader.readConfig(configPath);
        googleService = new GoogleService(configDto.getGoogleCredentialPath());
    }

    public void run() {
        for(var backupConfig : configDto.getBackups()) {
            fsService.setRootFilePath(backupConfig.getFileSystemPath());
            googleService.setRootFilePath(backupConfig.getGoogleFolderId());
            fileSynchronizer.setFromService(fsService);
            fileSynchronizer.setToService(googleService);

            int retryCount = 0;
            while(true){
                try{
                    fileSynchronizer.sync(backupConfig.getName(), backupConfig.getCleanBeforeBackup());
                    break;
                } catch (Exception e) {
                    if(retryCount++ >= configDto.getMaxRetries()) {
                        LOG.error("Failed to sync",e);
                        return;
                    }
                    else{
                        LOG.error("Failed to sync, retrying {}", retryCount, e);
                    }
                }
            }
        }
    }
}
