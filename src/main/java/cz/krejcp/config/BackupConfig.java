package cz.krejcp.config;

import lombok.Data;

@Data
public class BackupConfig {

    private String name;
    private String fileSystemPath;
    private String googleFolderId;
    private Boolean cleanBeforeBackup;

}
