package cz.krejcp.config;

import lombok.Data;

import java.util.List;

@Data
public class ConfigDto {
    private String googleCredentialPath;
    private Integer maxRetries;
    private List<BackupConfig> backups;

}
