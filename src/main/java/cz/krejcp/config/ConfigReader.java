package cz.krejcp.config;

import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ConfigReader {

    public ConfigDto readConfig(String path) throws IOException {
        Yaml yaml = new Yaml();
        return yaml.loadAs(Files.readString(Path.of(path)), ConfigDto.class);
    }
}
