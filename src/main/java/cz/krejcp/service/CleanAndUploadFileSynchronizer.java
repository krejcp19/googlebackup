package cz.krejcp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.OperationNotSupportedException;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;

public class CleanAndUploadFileSynchronizer extends FileSynchronizer {

    private static final Logger LOG = LoggerFactory.getLogger(CleanAndUploadFileSynchronizer.class);

    @Override
    public void sync(String configName, boolean cleanBeforeBackup) throws GeneralSecurityException, IOException, OperationNotSupportedException {
        LOG.info("Backup for '{}' starting: {}", configName, LocalDateTime.now());
        if(cleanBeforeBackup){
            deleteAllFilesFromTarget();
        }
        SyncFile fromFile = fromService.getRootFile();
        SyncFile toFolder = toService.getRootFile();
        uploadDirectoryContent(fromFile, toFolder);
        LOG.info("Backup for '{}' ending: {}", configName, LocalDateTime.now());
    }

    private void deleteAllFilesFromTarget() throws IOException, GeneralSecurityException, OperationNotSupportedException {
        SyncFile rootFile = toService.getRootFile();
        toService.fillContent(rootFile);
        for(SyncFile child: rootFile.getContent()) {
            toService.removeFile(child);
        }
    }

    private void uploadDirectoryContent(SyncFile fromFolder, SyncFile toFolder) throws IOException, GeneralSecurityException, OperationNotSupportedException {
        fromService.fillContent(fromFolder);
        for(SyncFile child : fromFolder.getContent()) {
            if(child.isDirectory()){
                SyncFile newFolder = toService.createDirectory(child.getName(), toFolder.getAbsolutePath());
                uploadDirectoryContent(child, newFolder);
            }
            else{
                toService.saveFile(new File(child.getAbsolutePath()), toFolder.getAbsolutePath());
            }
        }
    }
}
