package cz.krejcp.service;

import cz.krejcp.service.file.FileService;

import javax.naming.OperationNotSupportedException;
import java.io.IOException;
import java.security.GeneralSecurityException;

public abstract class FileSynchronizer {

    protected FileService fromService;
    protected FileService toService;

    public abstract void sync(String configName, boolean cleanBeforeBackup) throws GeneralSecurityException, IOException, OperationNotSupportedException;

    public void setFromService(FileService fromService) {
        this.fromService = fromService;
    }

    public void setToService(FileService toService) {
        this.toService = toService;
    }

}
