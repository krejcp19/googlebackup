package cz.krejcp.service;

import java.time.LocalTime;
import java.util.List;

public class SyncFile {
    private String name;
    private String absolutePath;
    private LocalTime lastUpdate;
    private Long size;
    private boolean isDirectory;
    private List<SyncFile> content;

    public SyncFile() {}

    public SyncFile(String name, String absolutePath, LocalTime lastUpdate, Long size, boolean isDirectory) {
        this.name = name;
        this.absolutePath = absolutePath;
        this.lastUpdate = lastUpdate;
        this.size = size;
        this.isDirectory = isDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public void setDirectory(boolean directory) {
        isDirectory = directory;
    }

    public List<SyncFile> getContent() {
        return content;
    }

    public void setContent(List<SyncFile> content) {
        this.content = content;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }
}
