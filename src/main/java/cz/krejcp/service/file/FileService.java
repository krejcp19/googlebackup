package cz.krejcp.service.file;

import cz.krejcp.service.SyncFile;

import javax.naming.OperationNotSupportedException;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

public interface FileService {

    SyncFile getRootFile() throws IOException, GeneralSecurityException;
    List<SyncFile> fillContent(SyncFile file) throws IOException, GeneralSecurityException;
    void removeFile(SyncFile file) throws OperationNotSupportedException, GeneralSecurityException, IOException;
    SyncFile saveFile(File file, String parentPath) throws OperationNotSupportedException, GeneralSecurityException, IOException;
    SyncFile createDirectory(String dirName, String parentId) throws OperationNotSupportedException, GeneralSecurityException, IOException;
    void setRootFilePath(String path);
}
