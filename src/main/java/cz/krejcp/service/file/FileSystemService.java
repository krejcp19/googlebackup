package cz.krejcp.service.file;

import cz.krejcp.service.SyncFile;

import javax.naming.OperationNotSupportedException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class FileSystemService implements FileService {

    private String rootFilePath;

    @Override
    public SyncFile getRootFile() throws FileNotFoundException {
        return fetchSyncFileFromPath(rootFilePath);
    }

    @Override
    public List<SyncFile> fillContent(SyncFile file) throws IOException {
        List<SyncFile> content = new ArrayList<>();
        List<Path> contentPaths = new ArrayList<>();
        Files.list(new File(file.getAbsolutePath()).toPath()).forEach(
                contentPaths::add
        );
        for(Path path : contentPaths) {
            content.add(fetchSyncFileFromPath(path.toAbsolutePath().toString()));
        }
        file.setContent(content);
        return content;
    }

    @Override
    public void removeFile(SyncFile file) throws OperationNotSupportedException {
        throw new OperationNotSupportedException();
    }

    @Override
    public SyncFile saveFile(File file, String parentPath) throws OperationNotSupportedException {
        throw new OperationNotSupportedException();
    }

    @Override
    public SyncFile createDirectory(String dirName, String parentId) throws OperationNotSupportedException {
        throw new OperationNotSupportedException();
    }

    private SyncFile fetchSyncFileFromPath(String path) throws FileNotFoundException {
        File file = new File(path);
        if(!file.exists()) {
            throw new FileNotFoundException("File " + rootFilePath + " was not found");
        }
        String name = file.getName();
        String absolutePath = file.getAbsolutePath();
        boolean isDirectory = file.isDirectory();
        LocalTime lastModified = LocalTime.ofInstant(Instant.ofEpochMilli(file.lastModified()), ZoneId.systemDefault());
        Long size = file.length();
        return new SyncFile(name, absolutePath, lastModified, size, isDirectory);
    }

    @Override
    public void setRootFilePath(String path) {
        rootFilePath = path;
    }
}
