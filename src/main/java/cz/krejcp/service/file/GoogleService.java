package cz.krejcp.service.file;

import com.google.api.services.drive.model.File;
import cz.krejcp.service.SyncFile;
import cz.krejcp.service.repository.GoogleRepository;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class GoogleService implements FileService {

    private static final Logger LOG = LoggerFactory.getLogger(GoogleService.class);
    private String rootFileId;
    private final GoogleRepository repository;

    public GoogleService(String credentialsPath) throws GeneralSecurityException, IOException {
        repository = new GoogleRepository(credentialsPath);
    }

    @Override
    public SyncFile getRootFile() throws IOException {
        File rootFile = repository.getFile(rootFileId);
        return convertGoogleFileToSyncFile(rootFile);
    }

    @Override
    public List<SyncFile> fillContent(SyncFile file) throws IOException {
        List<File> googleContent = repository.findFilesInFolder(file.getAbsolutePath());
        List<SyncFile> content = new ArrayList<>();
        for(File fileIt : googleContent) {
            content.add(convertGoogleFileToSyncFile(fileIt));
        }
        file.setContent(content);
        return content;
    }

    @Override
    public void removeFile(SyncFile file) throws IOException {
        if(file.isDirectory()) {
            List<SyncFile> children = fillContent(file);
            for(SyncFile child : children){
                removeFile(child);
            }
        }
        LOG.info("deleting {}: {} ({})", file.isDirectory()?"folder":"file", file.getName(), file.getAbsolutePath());
        repository.removeFile(file.getAbsolutePath());
    }

    @Override
    public SyncFile saveFile(java.io.File file, String parentPath) throws IOException {
        LOG.info("Uploading file: {} ({}) to folder {}", file.getAbsolutePath(), FileUtils.byteCountToDisplaySize(file.length()), parentPath);
        return convertGoogleFileToSyncFile(repository.uploadFile(file, parentPath));
    }

    @Override
    public SyncFile createDirectory(String dirName, String parentId) throws IOException {
        LOG.info("Created folder: {} in folder {}", dirName, parentId);
        return convertGoogleFileToSyncFile(repository.createDirectory(dirName, parentId));
    }

    private SyncFile convertGoogleFileToSyncFile(File file) {
        LocalTime lastModified = LocalTime.ofInstant(Instant.ofEpochMilli(file.getModifiedTime().getValue()), ZoneId.systemDefault());
        SyncFile syncFile = new SyncFile();
        syncFile.setName(file.getName());
        syncFile.setAbsolutePath(file.getId());
        syncFile.setDirectory(file.getMimeType().equals("application/vnd.google-apps.folder"));
        syncFile.setLastUpdate(lastModified);
        syncFile.setSize(file.getSize());
        return syncFile;
    }

    @Override
    public void setRootFilePath(String path) {
        rootFileId = path;
    }

}
