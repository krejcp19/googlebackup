package cz.krejcp.service.repository;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

public class GoogleRepository {

    private static final String APPLICATION_NAME = "Google backup";
    private static final String REQUEST_FIELDS = "id, name, mimeType , size, modifiedTime";
    private static final List<String> scopes = List.of(DriveScopes.DRIVE);

    private final Drive driveRepository;

    private final String credentialsPath;

    public GoogleRepository(String credentialsPath) throws GeneralSecurityException, IOException {
        this.credentialsPath = credentialsPath;
        JsonFactory jsonFactory = GsonFactory.getDefaultInstance();
        NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        com.google.api.services.drive.Drive.Builder builder =
                new Drive.Builder(httpTransport, jsonFactory,
                        request -> {
                            getCredentials().initialize(request);
                            request.setConnectTimeout(5*60_000);
                            request.setReadTimeout(5*60_000);
                        }
                );
        builder.setApplicationName(APPLICATION_NAME);
        driveRepository = builder.build();
    }

    public File createFolder(String folderName, String parentId) throws IOException {
        File newFolder = new File();
        newFolder.setName(folderName);
        newFolder.setMimeType("application/vnd.google-apps.folder");
        newFolder.setParents(Collections.singletonList(parentId));

        return driveRepository.files().create(newFolder).setFields("id").execute();
    }

    public List<File> findFilesInFolder(String folderId) throws IOException {
        String toolFolderQuery = String.format("'%s' in parents", folderId);
        FileList toolFolderList = driveRepository.files().list()
            .setFields("files(id, name, mimeType , size, modifiedTime)")
            .setQ(toolFolderQuery)
            .execute();
        return toolFolderList.getFiles();
    }

    public File uploadFile(java.io.File file, String parentId) throws IOException {
        File fileMetadata = new File();
        fileMetadata.setName(file.getName());
        fileMetadata.setParents(Collections.singletonList(parentId));
        FileContent mediaContent = new FileContent("image/jpeg", file);
        return driveRepository.files().create(fileMetadata, mediaContent)
            .setFields(REQUEST_FIELDS)
            .execute();
    }

    public void removeFile(String fileId) throws IOException {
        driveRepository.files().delete(fileId).execute();
    }

    public File getFile(String fileId) throws IOException {
        return driveRepository.files().get(fileId).setFields(REQUEST_FIELDS).execute();
    }

    public File createDirectory(String name, String parentId) throws IOException {
        File newFolder = new File();
        newFolder.setName(name);
        newFolder.setMimeType("application/vnd.google-apps.folder");
        newFolder.setParents(Collections.singletonList(parentId));

        return driveRepository.files().create(newFolder).setFields(REQUEST_FIELDS).execute();
    }

    private Credential getCredentials() throws IOException {
        return GoogleCredential.fromStream(getCredentialsStream()).createScoped(scopes);
    }

    private InputStream getCredentialsStream() throws IOException {
        return Files.newInputStream(Paths.get(credentialsPath));
    }
}
